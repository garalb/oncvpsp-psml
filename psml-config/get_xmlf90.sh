#!/bin/sh
#
XMLF90_PKG=xmlf90-1.5.0.tgz
XMLF90_URI=https://launchpad.net/xmlf90/trunk/1.5/+download/xmlf90-1.5.0.tgz
#
curl -L -o ${XMLF90_PKG} ${XMLF90_URI}
echo "-"
echo "Successfully downloaded the xmlf90 library tarball: ${XMLF90_PKG}"
echo "Unpack and follow the instructions in INSTALL to build the library"
